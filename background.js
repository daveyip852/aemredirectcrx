chrome.browserAction.onClicked.addListener(function (tab) { //Fired when User Clicks ICON
	console.log(tab.url);
    if (tab.url.indexOf("http://localhost:4502/") != -1) { // Inspect whether the place where user clicked matches with our list of URL
        chrome.tabs.executeScript(tab.id, {
            code: 'var url = '+ JSON.stringify(tab.url)
        }, function () { // Execute your code
            chrome.tabs.executeScript(tab.id, {file: 'contentscript.js'});
        });
    }
});